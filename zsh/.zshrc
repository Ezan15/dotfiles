# Helper include function
function _include {
    if [ -f "$HOME/.config/zsh/$1" ]; then
        source "${HOME}/.config/zsh/$1"
    elif [ -f "$1" ]; then
        source $1
    else
        1>&2 echo "$1 does not exist"
    fi
}

_include aliases
_include bindings
_include completion
_include fuzzy
_include prompt
_include shellopts
_include /etc/zsh_command_not_found

unfunction _include

# vim: path=.,~/.config/zsh,zsh
