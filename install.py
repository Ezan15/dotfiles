#!/usr/bin/env python3
"""Install dotfiles from arguments according to modules.json config"""

import sys
import json
import subprocess
import argparse
from pathlib import Path


def die(msg):
    """Print error message and exit with failure"""
    print("Error: {}".format(msg), file=sys.stderr)
    sys.exit(1)


def load_modules(modules_to_load):
    """Load module configuration"""
    with open('modules.json') as json_modules:
        module_config = json.load(json_modules)
        if modules_to_load[0] == "all":
            return module_config
        filtered_modules = {}
        for module_to_load in modules_to_load:
            if module_to_load not in module_config:
                die("Module {} does not exist".format(module_to_load))
            filtered_modules[module_to_load] = module_config[module_to_load]
        return filtered_modules


def install_module(name, module_config):
    """Install given module"""
    print("Installing module {}".format(name))
    if "directories" in module_config:
        create_directories(module_config["directories"])
    if "links" in module_config:
        create_links(module_config["links"], name)
    if "repositories" in module_config:
        create_repositories(module_config["repositories"])
    if "postcommands" in module_config:
        execute_commands(module_config["postcommands"])


def create_directories(directory_list):
    """Create list of directories with parents"""
    print("\tCreating directories : {}".format(directory_list))
    for directory in directory_list:
        directory_path = Path.home() / directory
        if not directory_path.exists():
            print("\t\tCreating directory : {}".format(directory_path))
            directory_path.mkdir(parents=True)


def create_links(link_dict, name):
    """Create list of symbolic links (clobbers existing files)"""
    print("\tCreating links : {}".format(link_dict))
    for link_source, link_target in link_dict.items():
        src = (Path(__file__).parent / name / link_source).resolve()
        dst = (Path.home() / link_target).resolve()
        if dst.is_dir():
            dst = dst / src.name
        if dst.exists():
            print("\t\tRemoving {}".format(dst))
            dst.unlink()
        print("\t\tLinking {1} to {0}".format(src, dst))
        dst.symlink_to(src)


def create_repositories(repository_dict):
    """Clone repositories (shallowly) in given path"""
    print("\tCloning repositories : {}".format(repository_dict))
    for repo_source, repo_target in repository_dict.items():
        dst = Path.home() / repo_target
        print("\t\tCloning repository {} in {}".format(repo_source, repo_target))
        subprocess.call(["git", "clone", "--depth=1", repo_source, str(dst)])


def execute_commands(command_list):
    """Execute command list"""
    print("\tExecuting commands : {}".format(command_list))
    for command in command_list:
        print("\t\tExecuting command : {}".format(command))
        subprocess.call(command)


if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description="Install dotfiles")
    PARSER.add_argument("modules", metavar="MODULE", nargs="+", help="Modules to install")
    ARGS = PARSER.parse_args()

    MODULE_CONFIG = load_modules(ARGS.modules)
    for module_name, module in MODULE_CONFIG.items():
        install_module(module_name, module)
