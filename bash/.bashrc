# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

[ -f ~/.profile ] && . ~/.profile

###############################
######### History #############
###############################
HISTCONTROL=ignoreboth
HISTSIZE=1000
HISTFILESIZE=2000
shopt -s histappend
set +H

###############################
########## Window #############
###############################
shopt -s checkwinsize


###############################
######### Globs ###############
###############################
shopt -s globstar

###############################
##### Friendly commands #######
###############################
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
[ -f ~/.bash_aliases ] && . ~/.bash_aliases

###############################
########## Prompt #############
###############################
__PROMPT_COMMANDSTARTDATE=$(date +%s)
exec_info () {
    if [ "$fromPrompt" == true ]; then
        __PROMPT_COMMANDSTARTDATE=$(date +%s)
    fi
    fromPrompt=false # Prevent chained commands from being trapped more than once
}

declare -A last_git_status
git_parsed_info () {
    # Original from https://github.com/magicmonty/bash-git-prompt/blob/master/gitstatus.sh (BSD-2-Clause “Simplified” License, https://github.com/magicmonty/bash-git-prompt/blob/master/LICENSE.txt)
    local gitstatus=$(git status --porcelain --branch)
    local num_conflicts=0
    local num_untracked=0
    local num_changed=0
    local branch_line=0
    local num_staged=0
    local num_stashed=0
    local clean=0
    while IFS='' read -r line || [[ -n "$line" ]]; do
        status=${line:0:2}
        while [[ -n $status ]]; do
            case "$status" in
                #two fixed character matches, loop finished
                \#\#) break ;;
                \?\?) ((num_untracked++)); break ;;
                U?) ((num_conflicts++)); break;;
                ?U) ((num_conflicts++)); break;;
                DD) ((num_conflicts++)); break;;
                AA) ((num_conflicts++)); break;;
                #two character matches, first loop
                ?M) ((num_changed++)) ;;
                ?D) ((num_changed++)) ;;
                ?\ ) ;;
                #single character matches, second loop
                U) ((num_conflicts++)) ;;
                \ ) ;;
                *) ((num_staged++)) ;;
            esac
            status=${status:0:(${#status}-1)}
        done
    done <<< "${gitstatus}"
    local stash_file="$( git rev-parse --git-dir )/logs/refs/stash"
    if [[ -e "${stash_file}" ]]; then
        while IFS='' read -r wcline || [[ -n "$wcline" ]]; do
            ((num_stashed++))
        done < ${stash_file}
    fi
    if (( num_changed == 0 && num_staged == 0 && num_untracked == 0 && num_stashed == 0 && num_conflicts == 0)) ; then
        clean=1
    fi
    last_git_status=(
        ["conflicts"]=${num_conflicts}
        ["untracked"]=${num_untracked}
        ["changed"]=${num_changed}
        ["staged"]=$(($num_staged))
        ["stashed"]=${num_stashed}
        ["clean"]=${clean}
        ["ahead"]=$(echo ${gitstatus} | head -n1 | grep --only-matching --perl-regexp '(?<=ahead )\d+' || echo 0)
        ["behind"]=$(echo ${gitstatus} | head -n1 | grep --only-matching --perl-regexp '(?<=behind )\d+' || echo 0)
    )
}

ALLOW_FETCH_ON_PROMPT=1
PROMPT_FETCH_TIMEOUT=60
__lastfetchdate=0
__fetching=0
set_prompt () {
    local lastCommand=$? # Must come first!
    PS1=""
    local old=0
    function set_prompt__new_section {
        local new=$1
        PS1+="\[\e[4${new}m\e[3${old}m\]\[\e[4${new}m\e[37m\]"
        old="$new"
    }

    local execTime=$(($(date +%s) - $__PROMPT_COMMANDSTARTDATE))
    if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
        set_prompt__new_section 5
        PS1+="\h"
    fi
    # If last command failed, print a red X.
    if [[ $lastCommand != 0 ]]; then
        set_prompt__new_section 1
        PS1+="$lastCommand"
    fi
    if [[ "$execTime" > 1 ]]; then
        set_prompt__new_section 4
        PS1+="$execTime"
    fi

    set_prompt__new_section 6
    PS1+="\w"  # Working directory
    if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) ]]; then # show git branch if in git subtree
        if [[ "${ALLOW_FETCH_ON_PROMPT}" -eq 1 && "$(($(date +%s) - ${__lastfetchdate}))" -gt "${PROMPT_FETCH_TIMEOUT}" && "${__fetching}" -eq 0 ]] ; then
            __lastfetchdate=$(date +%s)
            __fetching=1
            (git fetch --quiet && __fetching=0 &)
        fi
        set_prompt__new_section 3
        local branch=$(git rev-parse --abbrev-ref HEAD 2> /dev/null)
        if [[ "${branch}" == "HEAD" ]]; then
            PS1+=":$(git rev-parse --short HEAD 2> /dev/null)"
        else
            PS1+="${branch}"
        fi
        git_parsed_info
        if [[ "${last_git_status[ahead]}" -ne 0 ]]; then
            PS1+="↑ ${last_git_status[ahead]}"
        fi
        if [[ "${last_git_status[behind]}" -ne 0 ]]; then
            PS1+="↓ ${last_git_status[behind]}"
        fi
        set_prompt__new_section 2
        if [[ "${last_git_status[changed]}" -ne 0 ]]; then
            PS1+="✚ ${last_git_status[changed]}"
        fi
        if [[ "${last_git_status[staged]}" -ne 0 ]]; then
            PS1+="● ${last_git_status[staged]}"
        fi
        if [[ "${last_git_status[stashed]}" -ne 0 ]]; then
            PS1+="⚑ ${last_git_status[stashed]}"
        fi
        if [[ "${last_git_status[conflicts]}" -ne 0 ]]; then
            PS1+="✖ ${last_git_status[conflicts]}"
        fi
        if [[ "${last_git_status[untracked]}" -ne 0 ]]; then
            PS1+="…${last_git_status[untracked]}"
        fi
        if [[ "${last_git_status[clean]}" -eq 1 ]]; then
            PS1+="✔ "
        fi
    fi
    set_prompt__new_section 0
    PS1+="\[\e[0m\] "

    local abridged_pwd=$(perl -p -e "s|^$HOME|~|;s|([^/])[^/]*/|$""1/|g" <<< $PWD)
    local new_title="\e]2;$abridged_pwd\007"
    PS1+="\[$new_title\]"
    fromPrompt=true
}
trap 'exec_info' DEBUG
PROMPT_COMMAND='set_prompt'

###############################
######## Completion ###########
###############################
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
  if command -v pandoc > /dev/null 2>&1; then
      eval "$(pandoc --bash-completion)"
  fi
fi

