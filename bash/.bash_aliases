function mkcd
{
    mkdir $1 -p;
    cd $1;
}

function pinguntilsuccess
{
    while ! ping -c1 "$1";
    do
        date;
    done;
    echo "Ping succeeded"
}

alias oops="history -d \$(history | tail -n2 | head -n1 | tr -s ' ' | cut -d' ' -f 2); history -w"


alias commandofwindow="ps -o command \"\$(xprop _NET_WM_PID | cut -d' ' -f3)\" | tail -n +2"

alias x="startx"

# Watch video from /tmp
# Useful if /tmp is mounted as tmpfs and video is on a slow drive
function tmpvlc
{
    tmppath=$(mktemp)
    cp "$1" "$tmppath"
    vlc "$tmppath"
    rm -f "$tmppath"
}

function viman
{
    view -c 'set readonly nomodifiable filetype=man foldmethod=indent' <(man $@)
}

# Quick docker
if which docker > /dev/null 2>&1 ; then
    alias qdck="docker run --rm -it"
    alias ds='docker search -f is-official=true --format "table {{.Name}} (★ {{.StarCount}}):\t{{.Description}}" --no-trunc'
fi

alias nodegrep="grep --exclude-dir={libs,node_modules,.git,platforms,www} --exclude={*.min.*,bundle.js,bundle.js.map,*.svg,*.css,*.scss,Session.vim,.*}"

alias ll='ls -AlFh'
alias l='ls -F'

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

function haste
{
    file=$(cat $@)
    url=$(curl -X POST -s -d "$file" https://bin.vgaudard.com/documents | awk -F '"' '{print "https://bin.vgaudard.com/"$4}')
    echo "${url}" | xsel --clipboard
    echo "Yanked URL : ${url}"
}
