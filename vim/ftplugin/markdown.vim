" Spelling
setlocal spell spelllang=en,fr

" Text formatting
setlocal textwidth=80

let b:undo_ftplugin = 'setlocal spell< spelllang< textwidth<'
