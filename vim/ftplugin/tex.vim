" Text formatting
setlocal textwidth=80 colorcolumn=81

" spell checking
setlocal spell spelllang=en,fr
