" Spelling
setlocal spell spelllang=en

" Text formatting
setlocal textwidth=72

let b:undo_ftplugin = 'setlocal spell< spelllang< textwidth<'
