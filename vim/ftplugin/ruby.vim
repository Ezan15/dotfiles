" Abbreviations
iabbrev <buffer> #! #!/usr/bin/ruby

" Folding
setlocal foldmethod=indent
call functions#setfoldleveltomax()

let b:undo_ftplugin = 'setlocal foldmethod< foldlevel<'
    \ . '| iunabbrev <buffer> #!'
