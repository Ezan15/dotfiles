function! functions#setfoldleveltomax()
    let &foldlevel = max(map(range(1, line('$')), 'foldlevel(v:val)'))
endfunction

function! functions#jumptolastposition()
    if line('''"') >= 1 && line('''"') <= line('$') && &filetype !~# 'commit'
        execute 'normal! g''"'
    endif
endfunction

function! functions#reducetimeout()
    let s:old_timeoutlen = &l:timeoutlen
    setlocal timeoutlen=60
endfunction

function! functions#resettimeout()
    let &l:timeoutlen = s:old_timeoutlen
    unlet s:old_timeoutlen
endfunction

function! functions#installplug()
    function s:InstallPlugins()
        PlugInstall --sync
        autocmd! VimPlugInstall
        source $MYVIMRC
    endfunction
    echom 'Installing vim-plug'
    !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    augroup VimPlugInstall
        autocmd!
        autocmd VimEnter * call s:InstallPlugins()
    augroup END
endfunction

" Go to file (vim) or web page (browser) under cursor
function! functions#gf()
    let url = expand('<cfile>')
    if match(url, '^\Chttps?://')
        silent execute "!xdg-open '" . shellescape(url) . "'"
    else
        15verbose normal! gf
    endif
endfunction

" vim:tw=80 cc=81
