scriptencoding utf-8

" Plugins {{{
    " Auto install vim-plug
    if empty(glob('~/.vim/autoload/plug.vim'))
        call functions#installplug()
    endif

    call plug#begin('~/.vim/plugged')

    " Surround.vim is all about "surroundings": parentheses, brackets, quotes,
    " XML tags, and more.
    Plug 'tpope/vim-surround'

    " repeat.vim: enable repeating supported plugin maps with "."
    Plug 'tpope/vim-repeat'

    " Lean & mean status/tabline for vim that's light as air.
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    " {{{
        set laststatus=2
        let g:airline_powerline_fonts = 1
    " }}}

    " Asynchronous linting/fixing for Vim and Language Server Protocol (LSP)
    " integration
    Plug 'w0rp/ale'
    " {{{
        let g:airline#extensions#ale#enabled = 1
        let g:ale_open_list = 1
    " }}}

    " endwise.vim is a simple plugin that helps to end certain structures
    " automatically
    Plug 'tpope/vim-endwise'

    " Vim script for text filtering and alignment
    Plug 'godlygeek/tabular'

    " Vim plugin that provides additional text objects
    Plug 'wellle/targets.vim'

    " precision colorscheme for the vim text editor
    Plug 'altercation/vim-colors-solarized'

    " Markdown Vim Mode http://plasticboy.com/markdown-vim-mode/
    Plug 'plasticboy/vim-markdown'

    " A modern vim plugin for editing LaTeX files.
    Plug 'lervag/vimtex'

    " vinegar.vim: Combine with netrw to create a delicious salad dressing
    Plug 'tpope/vim-vinegar'

    " eunuch.vim: helpers for UNIX
    Plug 'tpope/vim-eunuch'

    " The ultimate undo history visualizer for VIM
    Plug 'mbbill/undotree'

    " vim: interpret a file by function and cache file automatically
    Plug 'MarcWeber/vim-addon-mw-utils'

    " Some utility functions for VIM
    Plug 'tomtom/tlib_vim'

    " snipMate.vim aims to be a concise vim script that implements some of
    " TextMate's snippets features in Vim
    Plug 'garbas/vim-snipmate'

    " vim-snipmate default snippets (Previously snipmate-snippets)
    Plug 'honza/vim-snippets'

    " List of JavaScript ES6 snippets and syntax highlighting for vim.
    Plug 'isRuslan/vim-es6'

    " A code-completion engine for Vim
    Plug 'Valloric/YouCompleteMe', { 'do': './install.py' }

    " A command-line fuzzy finder
    Plug 'junegunn/fzf', {
        \ 'dir': '~/.fzf',
        \ 'do': './install --key-bindings --completion --no-update-rc --no-bash --no-fish --xdg'
        \ }

    " sleuth.vim: Heuristically set buffer options
    Plug 'tpope/vim-sleuth'

    " abolish.vim: easily search for, substitute, and abbreviate multiple
    " variants of a word
    Plug 'tpope/vim-abolish'

    " dispatch.vim: Asynchronous build and test dispatcher
    Plug 'tpope/vim-dispatch'

    call plug#end()
" }}}

" Appearance {{{
    set background=dark
    colorscheme solarized
" }}}

" Options {{{
    set showcmd         " Show (partial) command in status line.
    set autowrite       " Automatically save before commands like :next and
                        " :make
    set hidden          " Hide buffers when they are abandoned
    set mouse=a
    set textwidth=0
    syntax on
    set modeline
    set timeout
    set timeoutlen=300
    set backspace=2     " Backspace behaves in a sane way
    set formatoptions-=cro
    set colorcolumn=+1
    set nrformats-=octal

    " Search options
    set ignorecase      " Do case insensitive matching
    set smartcase       " Do smart case matching
    set incsearch       " Incremental search
    set hlsearch        " Highlight search results

    " Indenting options
    set tabstop=4
    set shiftwidth=4    " Number of spaces to use for each step of (auto)indent
    set softtabstop=4
    set smarttab
    set expandtab
    set autoindent
    set shiftround      " Round indent to multiple of 'shiftwidth'
    set nojoinspaces

    " Text-wrapping
    set wrap
    set breakindent breakindentopt=sbr showbreak=↪
    set display=truncate

    " Line number
    set relativenumber number

    " Completion
    set infercase
    set wildmenu
    set wildmode=longest:full,full

    " Vim file management
    set backup          " keep a backup file (restore to previous version)
    set undofile        " keep an undo file (undo changes after closing)
    set backupdir=~/.vim/backup//
    set directory=~/.vim/swap//
    set undodir=~/.vim/undo//
    set undolevels=10000
" }}}

" Mappings {{{
    " Remapping for an AZERTY keyboard
    nnoremap ù .
    nnoremap é ~
    vnoremap é ~
    nnoremap à @
    nnoremap àà @@
    vnoremap à @
    vnoremap àà @@
    nnoremap ' `
    vnoremap ' `

    " Leader keys
    let mapleader="\<Space>"
    let maplocalleader='è'

    " Window navigation
    nnoremap <Leader>h        <c-w>h
    nnoremap <Leader>j        <c-w>j
    nnoremap <Leader>k        <c-w>k
    nnoremap <Leader>l        <c-w>l
    nnoremap <Leader>H        <c-w>H
    nnoremap <Leader>J        <c-w>J
    nnoremap <Leader>K        <c-w>K
    nnoremap <Leader>L        <c-w>L

    " Quickfix
    nnoremap <Leader>fn     :cnext<Return>
    nnoremap <Leader>fp     :cprevious<Return>
    nnoremap <Leader>fc     :cclose<Return>
    nnoremap <Leader>fo     :copen<Return>

    " Grep
    nnoremap <silent> <Leader>* :silent grep -R <cword> .<Return>:copen<Return>

    " Quit insert mode when `jk` or `kj` pressed
    inoremap jk <Esc>
    inoremap kj <Esc>
    augroup InsertTimeoutlen
        autocmd!
        autocmd InsertEnter * call functions#reducetimeout()
        autocmd InsertLeave * call functions#resettimeout()
    augroup end

    nnoremap <silent>  <Leader>w   :update<Return>
    nnoremap <silent>  <Leader>q   :q<Return>

    " Quick edit/source files
    nnoremap <Leader>ee  :vsplit<Space>
    nnoremap <Leader>ev  :vsplit $MYVIMRC<Return>
    nnoremap <Leader>EE  :split<Space>
    nnoremap <Leader>rv  :source $MYVIMRC<Return>

    " Use leader for system clipboard
    nnoremap <Leader>y  "+y
    nnoremap <Leader>d  "+d
    vnoremap <Leader>y  "+y
    vnoremap <Leader>d  "+d
    nnoremap <Leader>p  "+p
    nnoremap <Leader>P  "+P

    " Copy all buffer to system clipboard
    nnoremap <Leader>v  ggvG$"+y''

    " Quick settings
    " Toggle line numbers quickly
    nnoremap <Leader>bn  :set number! relativenumber!<Return>

    " Toggle line wrap
    nnoremap <Leader>bw  :set wrap!<Return>

    " Remove highlight
    nnoremap <Leader>bl :nohlsearch<Return>

    " Target cursor
    nnoremap <Leader>bt :set cursorline! cursorcolumn!<Return>

    " gf works with links
    nnoremap <silent> gf :call functions#gf()<Return>
" }}}

" Autocommands {{{
    " Jump to the last position when reopening a file
    augroup last_position
        autocmd!
        autocmd BufReadPost * call functions#jumptolastposition()
    augroup end
" }}}

" vim:tw=80 cc=81
